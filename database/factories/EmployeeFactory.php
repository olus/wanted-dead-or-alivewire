<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->username(),
            'name' => $this->faker->name(),
            'surname' => $this->faker->name(),
            'birthDate' => now()->subDays(rand(1, 50)),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }
}
