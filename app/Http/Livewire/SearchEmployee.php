<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;
use Psr\Log\LoggerInterface;

class SearchEmployee extends Component
{
    public const RESULT_LIST_ELEMENT_ID = 'search';

    public ?string $searchQuery = null;

    private LoggerInterface $logger;

    public function boot(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function updatedSearchQuery(string $searchQuery): void
    {
        $this->logger->info($this->searchQuery);
        $this->emitTo(
            'show-employee-list',
            'setSearchQuery',
            self::RESULT_LIST_ELEMENT_ID,
            $searchQuery
        );
    }

    public function render(): View
    {
        return view('livewire.search-employee');
    }
}
