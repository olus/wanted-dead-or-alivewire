<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class SearchField extends Component
{
    public ?string $query = null;

    public function render(): View
    {
        return view('livewire.search-field');
    }
}
