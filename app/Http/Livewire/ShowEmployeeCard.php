<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Illuminate\View\View;
use Livewire\Component;

class ShowEmployeeCard extends Component
{
    public Employee $employee;

    public function render(): View
    {
        return view('livewire.show-employee-card');
    }
}
