<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Faker\Generator;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use Livewire\Component;

class ShowEmployeeRow extends Component
{
    public Employee $employee;

    public function setRandomEmail(): void
    {
        $this->employee->email = App::make(Generator::class)->safeEmail();
        $this->employee->save();
        $this->emit('refreshEmployeeList');
    }

    public function render(): View
    {
        return view('livewire.show-employee-row');
    }
}
