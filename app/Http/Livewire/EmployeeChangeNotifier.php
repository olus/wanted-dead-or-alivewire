<?php

namespace App\Http\Livewire;

use App\Models\Employee;
use Livewire\Component;

class EmployeeChangeNotifier extends Component
{
    public int $count;

    public function checkActualEmployeeCount(): void
    {
        $newCount = $this->getCount();
        if ($newCount !== $this->count) {
            $this->emitTo(
                'main-message',
                'showMessage',
                'error',
                'Something has changed on the list'
            );
        }
        if ($newCount === 0) {
            redirect()->to('/empty-list');
        }
        $this->count = $newCount;
    }

    public function mount(): void
    {
        $this->count = $this->getCount();
    }

    private function getCount(): int
    {
        return Employee::count();
    }

    public function render()
    {
        return view('livewire.employee-change-notifier');
    }
}
