<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Illuminate\View\View;
use Livewire\Component;

class DeleteEmployee extends Component
{
    public bool $showStatus = false;

    public ?Employee $employee = null;

    protected $listeners = [
        'selectEmployeeToDelete' => 'selectEmployeeToDelete',
    ];

    public function deleteEmployee(): void
    {
        $this->showStatus = false;
        $this->employee->delete();

        $this->emit('refreshEmployeeList');
        $this->emitTo(
            'main-message',
            'showMessage',
            'success',
            sprintf('Employee "%s" has been deleted', $this->employee->username)
        );
    }

    public function selectEmployeeToDelete(Employee $employee): void
    {
        $this->employee = $employee;
        $this->showStatus = true;
    }

    public function render(): View
    {
        return $this->showStatus === false ?
            view('livewire.void-holder') :
            view('livewire.delete-employee');
    }
}
