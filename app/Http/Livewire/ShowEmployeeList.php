<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class ShowEmployeeList extends Component
{
    use WithPagination;

    public ?string $searchQuery = null;

    public ?string $elementId = null;

    public int $limit = 5;

    public string $viewType = 'list';

    protected $queryString = [
        'page' => ['except' => 1],
        'limit',
        'viewType',
    ];

    protected function getListeners(): array
    {
        return [
            'refreshEmployeeList' => '$refresh',
            'setSearchQuery' => 'setSearchQuery',
        ];
    }

    public function setSearchQuery(string $elementId, string $searchQuery): void
    {
        if ($elementId !== $this->elementId) {
            return;
        }
        $this->searchQuery = $searchQuery;
    }

    public function updatedLimit()
    {
        $this->resetPage();
    }

    private function getEmployeeResults(): LengthAwarePaginator
    {
        return Employee::where(function($query) {
            if (!is_null($this->searchQuery)) {
                $query->where('username', 'like', "%{$this->searchQuery}%");
            }
        })->orderBy('id', 'desc')
            ->paginate($this->limit);
    }

    public function render(): View
    {
        return view(
            $this->viewType === 'list' ?
                'livewire.show-employee-list' :
                'livewire.show-employee-cards',
            [
                'employees' => $this->getEmployeeResults(),
            ]
        );
    }
}
