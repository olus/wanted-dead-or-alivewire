<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Livewire\Component;

class PreviewEmployee extends Component
{
    public bool $showStatus = false;

    public ?Employee $employee = null;

    public ?int $employeeId = null;

    protected $queryString = ['employeeId'];

    protected $listeners = [
        'openModal' => 'selectEmployeeToPreview',
    ];

    public function mount(): void
    {
        if (is_null($this->employeeId)) {
            return;
        }
        $this->selectEmployeeToPreview(
            Employee::find($this->employeeId)
        );
    }

    public function selectEmployeeToPreview(Employee $employee): void
    {
        $this->employee = $employee;
        $this->employeeId = $this->employee->id;
        $this->showStatus = true;
    }

    public function render()
    {
        return $this->showStatus === false ?
            view('livewire.void-holder') :
            view('livewire.preview-employee');
    }
}
