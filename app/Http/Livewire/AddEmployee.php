<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Employee;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class AddEmployee extends Component
{
    use WithFileUploads;

    public const EMPLOYEE_PROPERTIES = [
        [
            'caption' => 'Username',
            'name' => 'username',
            'type' => 'text',
            'validation' => 'required|min:6|unique:employees,username',
        ],
        [
            'caption' => 'Name',
            'name' => 'name',
            'type' => 'text',
            'validation' => 'required',
        ],
        [
            'caption' => 'Surname',
            'name' => 'surname',
            'type' => 'text',
            'validation' => 'required',
        ],
        [
            'caption' => 'Email',
            'name' => 'email',
            'type' => 'email',
            'validation' => 'required|email',
        ],
        [
            'caption' => 'Birth date',
            'name' => 'birthDate',
            'type' => 'date',
            'validation' => 'required|date_format:Y-m-d',
        ],
        [
            'caption' => 'Photo',
            'name' => 'photo',
            'type' => 'file',
            'validation' => 'image|max:1024',
        ],
    ];

    public bool $showStatus = false;

    public ?string $username = null;
    public ?string $name = null;
    public ?string $surname = null;
    public ?string $email = null;
    public ?string $birthDate = null;
    public TemporaryUploadedFile|string|null $photo = null;

    public function toggleShowStatus(): void
    {
        $this->showStatus = !$this->showStatus;
    }

    public function updated(string $propertyName): void
    {
//        Preview:
//        'username', ['username' => 'required|min:6|unique:employees,username']
        $this->validateOnly(
            $propertyName,
            collect(self::EMPLOYEE_PROPERTIES)->where('name', $propertyName)
                ->pluck('validation', 'name')
                ->toArray()
        );
    }

    public function submit(): void
    {
//        Preview:
//        [
//            'username' => 'required|min:6|unique:employees,username',
//            'name' => 'required',
//            'surname' => 'required',
//            ...
//        ]
        $this->validate(
            collect(self::EMPLOYEE_PROPERTIES)->pluck('validation', 'name')
                ->toArray()
        );

        $photoPath = $this->photo->store('photos', ['disk' => 'public']);
        Employee::create(array_merge(
            $this->only(
                collect(self::EMPLOYEE_PROPERTIES)->pluck('name')
                    ->toArray()
            ),
            ['photo' => $photoPath],
        ));

        $this->reset();
        $this->emit('refreshEmployeeList');
        session()->flash('message', 'Employee has been added.');
    }

    public function render(): View
    {
        return view('livewire.add-employee', [
            'employeeProperties' => self::EMPLOYEE_PROPERTIES,
        ]);
    }
}
