<?php

namespace App\Http\Livewire;

use App\Models\Employee;
use Asantibanez\LivewireCalendar\LivewireCalendar;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class EmployeeCalendar extends LivewireCalendar
{
    public function events(): Collection
    {
        return Employee::all()->map(function($employee): array {
            return [
                'id' => $employee->id,
                'title' => $employee->username,
                'description' => $employee->name,
                'date' => $employee->birthDate,
            ];
        });
    }

    public function onEventDropped($eventId, $year, $month, $day)
    {
        Employee::find($eventId)->update([
            'birthDate' => Carbon::parse("{$year}-{$month}-{$day}")
        ]);
    }
}
