<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class MainMessage extends Component
{
    public ?string $type = null;

    public ?string $message = null;

    protected $listeners = [
        'showMessage' => 'handleMessage',
    ];

    public function hideMessageBox(): void
    {
        $this->message = null;
    }

    public function handleMessage(string $type, string $message): void
    {
        $this->type = $type;
        $this->message = $message;
    }

    public function render(): View
    {
        return is_null($this->message) ?
            view('livewire.void-holder') :
            view('livewire.main-message');
    }
}
