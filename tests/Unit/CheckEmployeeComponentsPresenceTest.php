<?php

namespace Tests\Unit;

use App\Http\Livewire\MainMessage;
use App\Http\Livewire\PreviewEmployee;
use Livewire\Livewire;
use Tests\TestCase;

class CheckEmployeeComponentsPresenceTest extends TestCase
{
    public function test_example()
    {
        $this->get('/employee')->assertSeeLivewire(MainMessage::class);
        $this->get('/employee')->assertSeeLivewire('search-employee');

        Livewire::withQueryParams(['employeeId' => 120])
            ->test(PreviewEmployee::class)
            ->assertSet('employeeId', '120')
            ->assertSee('mmmmmm');
    }
}
