<div class="table-row">
    <div class="table-cell p-2">
        <input type="checkbox">
    </div>
    <div class="table-cell p-2">{{ $employee->id }}</div>
    <div class="table-cell p-2">{{ $employee->username }}</div>
    <div class="table-cell p-2">{{ $employee->name }}</div>
    <div class="table-cell p-2">{{ $employee->surname }}</div>
    <div class="table-cell p-2">
        {{ $employee->email }}
        <button wire:click="setRandomEmail" class="bg-red-500 hover:bg-red-700 px-3 py-1 text-sm leading-5 rounded-md font-semibold text-white">Random</button>
    </div>
    <div class="table-cell p-2">{{ $employee->birthDate->format('Y-m-d') }}</div>
    <div class="table-cell p-2">
        <button wire:click="$emitTo('delete-employee', 'selectEmployeeToDelete', {{ $employee->id }})" class="bg-red-500 hover:bg-red-700 px-3 py-1 text-sm leading-5 rounded-md font-semibold text-white">Delete</button>
    </div>
</div>
