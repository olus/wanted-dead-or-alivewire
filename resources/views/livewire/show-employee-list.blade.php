<div>
    @include('livewire.parts.switch-view-type')

    <select wire:model="limit">
        <option>5</option>
        <option>10</option>
    </select>

    <div class="table w-full bg-white shadow rounded">
        <div class="table-header-group">
            <div class="table-row">
                <div class="table-cell p-2"></div>
                <div class="table-cell p-2">Id</div>
                <div class="table-cell p-2">Username</div>
                <div class="table-cell p-2">Name</div>
                <div class="table-cell p-2">Surname</div>
                <div class="table-cell p-2">Email</div>
                <div class="table-cell p-2">Birth date</div>
                <div class="table-cell p-2">Action</div>
            </div>
        </div>

        <div class="table-row-group">
        @foreach ($employees as $employee)
            <livewire:show-employee-row :employee="$employee" :wire:key="'employee-row-'.$employee->id.'-'.$employee->updated_at->timestamp" />
        @endforeach
        </div>
    </div>

    {{ $employees->links() }}
</div>
