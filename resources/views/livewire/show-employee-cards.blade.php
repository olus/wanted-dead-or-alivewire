<div>
    @include('livewire.parts.switch-view-type')

    <select wire:model="limit">
        <option>5</option>
        <option>10</option>
    </select>

    <div class="w-full flex flex-wrap">
    @foreach ($employees as $employee)
        <livewire:show-employee-card :employee="$employee" :wire:key="'employee-card-'.$employee->id.'-'.$employee->updated_at->timestamp" />
    @endforeach
    </div>

    {{ $employees->links() }}
</div>
