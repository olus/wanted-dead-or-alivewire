<div wire:click="$emitTo('preview-employee', 'openModal', {{ $employee }})" class="border-solid border-2 border-black-600 bg-white rounded m-1 p-2 w-1/4">
    <h3>#{{ $employee->id }} {{ $employee->username }}</h3>
    @if (!is_null($employee->photo))
        <img src="{{ $employee->photo_link }}">
    @endif
    <div class="text-xs">{{ $employee->name }} {{ $employee->surname }}</div>
</div>
