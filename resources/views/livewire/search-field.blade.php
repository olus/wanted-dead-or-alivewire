<div>
    <input type="text" wire:model="query" class="py-4">

    <div wire:loading.class.200s="border-2">Loading</div>
    <div>Searching for: {{ $query }}</div>
</div>


