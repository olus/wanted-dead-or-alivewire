<div>
    <input type="text" wire:model.debounce.500ms="searchQuery" class="w-full p-2 rounded" placeholder="Search for...">

    <div class="h-6">{{ !empty($searchQuery) ? sprintf('Searching for: "%s"', $searchQuery) : '' }}</div>
    <div class="h-6"><span class="hidden" wire:loading.inline>Loading</span></div>

    @if (!empty($searchQuery))
        <livewire:show-employee-list viewType="cards" :element-id="App\Http\Livewire\SearchEmployee::RESULT_LIST_ELEMENT_ID" />
    @endif
</div>
