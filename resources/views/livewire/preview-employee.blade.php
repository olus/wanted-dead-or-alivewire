@extends('livewire.parts.modal-layout')

@section('title')
    Employee <b>{{ $employee->username }}</b> preview
@endsection

@section('description')
    <img src="{{ $employee->photo_link }}" />

    @foreach ($employee->toArray() as $key => $val)
        <div>{{ $key }}: {{ $val }}</div>
    @endforeach
@endsection

@section('action-buttons')
    <button type="button" wire:click="$set('showStatus', false)" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">Close</button>
@endsection
