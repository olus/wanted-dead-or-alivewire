<div>
    <div wire:poll.6s="hideMessageBox" class="w-full justify-center rounded-md border border-transparent shadow-sm px-4 py-2 mt-2 mb-6 {{ $type === 'success' ? 'bg-green-300' : 'bg-red-300' }} text-xl text-white sm:w-auto">{{ $message }}</div>
</div>
