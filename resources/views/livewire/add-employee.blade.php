<div>
    <div class="max-w-sm smx-auto bg-white shadow py-5 px-6 mt-16">
        <h2 class="text-xl">Add new employee</h2>

    @if (session()->has('message'))
        <div class="text-green-500">
            {{ session('message') }}
        </div>
    @endif

    <button wire:click="toggleShowStatus" class="mt-6 bg-sky-500 hover:bg-sky-700 px-5 py-2.5 text-sm leading-5 rounded-md font-semibold text-white">{{ $showStatus === true ? 'Hide' : 'Show' }} form</button>

    @if ($showStatus)
        <form wire:submit.prevent="submit" class="animation-linear animation-duration-500 {{-- $showStatus === false ? 'animate-hide' : 'animate-show' --}}">

            @foreach ($employeeProperties as $field)
            <div class="mt-6">
                <label for="{{ $field['name'] }}" class="block text-sm font-medium text-slate-700">{{ $field['caption'] }}</label>
                <div class="mt-1">
                    <input type="{{ $field['type'] }}" name="{{ $field['name'] }}" id="{{ $field['name'] }}" wire:model.lazy="{{ $field['name'] }}" class="px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 invalid:border-pink-500 invalid:text-pink-600 focus:invalid:border-pink-500 focus:invalid:ring-pink-500 disabled:shadow-none" value="">
                </div>

                @if ($field['type'] === 'file' && ${$field['name']} !== null)
                    Preview:
                    <img src="{{ ${$field['name']}->temporaryUrl() }}">
                @endif

                @if ($errors->has($field['name']))
                    <span class="text-pink-500">{{ $errors->first($field['name']) }}</span>
                @endif
            </div>
            @endforeach

            <div class="mt-6 text-right">
                <button class="bg-sky-500 hover:bg-sky-700 px-5 py-2.5 text-sm leading-5 rounded-md font-semibold text-white">Save</button>
            </div>
        </form>
    @endif

    </div>
</div>
