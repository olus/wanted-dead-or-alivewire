@extends('layouts.employee')

@section('content')
<div class="w-9/12">
    <livewire:main-message />

    <livewire:search-employee />

    <h2 class="text-xl mt-12">All employees</h2>
    <livewire:show-employee-list />

    <div class="flex">
        <livewire:add-employee />
        <livewire:employee-change-notifier />
    </div>

    <livewire:delete-employee />
    <livewire:preview-employee />
</div>
@endsection
