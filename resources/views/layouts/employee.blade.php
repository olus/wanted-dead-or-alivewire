<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wanted dead or alivewire</title>

    <script src="https://cdn.tailwindcss.com"></script>

    <style>
    @keyframes animate-hide {
        0% {
            opacity: 1;
            height: 800px;
        }
        100% {
            height: 0px;
            opacity: 0;
        }
    }
    @keyframes animate-show {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

    .animation-linear {
        animation-timing-function: linear;
    }
    .animation-duration-500 {
        animation-duration: 0.5s;
    }
    .animation-delay-1000 {
        animation-delay: 1s;
    }
    .animate-show {
        animation-name: animate-show;
        animation-fill-mode: forwards;
    }
    .animate-hide {
        animation-name: animate-hide;
        animation-fill-mode: forwards;
    }
    </style>
    @livewireStyles

    @stack('styles')
</head>
<body>
<div class="flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    @yield('content')
</div>

@livewireScripts
</body>
</html>
