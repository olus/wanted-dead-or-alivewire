@extends('layouts.employee')

@push('styles')
    @livewireCalendarScripts
@endpush

@section('content')
    <div class="w-9/12">
        <livewire:employee-calendar initialYear="2022" initialMonth="1" />
    </div>
@endsection
